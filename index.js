require('dotenv').config();
const Discord = require('discord.js');
const bot = new Discord.Client();
bot.commands = new Discord.Collection();
const botCommands = require('./commands');
const SQLite = require("better-sqlite3");
const sqlCommandCount = new SQLite('./data/commandCount.sqlite');
const sqlSuggestion = new SQLite('./data/suggestions.sqlite');
var PREFIX = '!'

function recCount(message)
{
	var msg = message.toString();
	var recCounter = (msg.match(/:REC:/g) || []).length;
	msg = msg.split(':REC:').join('');
	msg = msg.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");
	msg = msg.toLowerCase();
	var msgArray = msg.split(' ');
	recCounter += msgArray.filter(function(x){ return x === "rec"; }).length;
	return recCounter;
}

Object.keys(botCommands).map(key =>
{
	bot.commands.set(botCommands[key].name, botCommands[key]);
});

const TOKEN = process.env.TOKEN;

bot.login(TOKEN);

bot.on('ready', () =>
{
	console.info(`${bot.user.tag} is online!`);

	// Check if the table exists.
	const commandCountTable = sqlCommandCount.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'commandCount';").get();
	if (!commandCountTable['count(*)'])
	{
		// If the table isn't there, create it and setup the database correctly.
		sqlCommandCount.prepare("CREATE TABLE commandCount (id TEXT PRIMARY KEY, command TEXT, uses INTEGER);").run();
		// Ensure that the "id" row is always unique and indexed.
		sqlCommandCount.prepare("CREATE UNIQUE INDEX idx_scores_id ON commandCount (id);").run();
		sqlCommandCount.pragma("synchronous = 1");
		sqlCommandCount.pragma("journal_mode = wal");
	}

	bot.getCommandCount = sqlCommandCount.prepare("SELECT * FROM commandCount WHERE command = ?");
	bot.setCommandCount = sqlCommandCount.prepare("INSERT OR REPLACE INTO commandCount (id, command, uses) VALUES (@id, @command, @uses);");
	
	/************************************************************/
	
	const suggestionTable = sqlSuggestion.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'suggestions';").get();
	if (!suggestionTable['count(*)'])
	{
		sqlSuggestion.prepare("CREATE TABLE suggestions (id TEXT PRIMARY KEY, suggestion TEXT, total INTEGER, date_last_suggested TEXT);").run();
		sqlSuggestion.prepare("CREATE UNIQUE INDEX idx_scores_id ON suggestions (id);").run();
		sqlSuggestion.pragma("synchronous = 1");
		sqlSuggestion.pragma("journal_mode = wal");
	}

	bot.getSuggestion = sqlSuggestion.prepare("SELECT * FROM suggestions WHERE suggestion = ?");
	bot.setSuggestion = sqlSuggestion.prepare("INSERT OR REPLACE INTO suggestions (id, suggestion, total, date_last_suggested) VALUES (@id, @suggestion, @total, @date_last_suggested);");
});

bot.on('message', message =>
{
	if(!message.content.startsWith(PREFIX) || message.author.bot)
		return;
	let args = message.content.trim().slice(PREFIX.length).split(/ +/g);
	const command = args.shift().toLowerCase();
	
	var totalREC = recCount(message);
	if(totalREC != 0)
	{
		let recCounter = bot.getCommandCount.get('rec');
		if(!recCounter)
		{
			recCounter = { id: `${message.id}-${'rec'}`, command: 'rec', uses: 0 };
		}
		recCounter.uses += totalREC;
		bot.setCommandCount.run(recCounter);
		
	}
	
	if (!bot.commands.has(command))
	{
		if(totalREC != 0)
			message.reply('<:REC:690020789109522462>');
		return;
	}
	else
	{
		if(!message.guild)
			if(!(command === 'suggest'))
				return;
	}
	
	try
	{
		//message.channel.send('Debug: command: ' + command);
		let count = bot.getCommandCount.get(command);
		if(!count)
		{
			count = { id: `${message.id}-${command}`, command: command, uses: 0 };
		}

		count.uses++;
		bot.setCommandCount.run(count);

		bot.commands.get(command).execute(message, args, bot);
	}
	catch(error)
	{
		console.error(error);
		message.reply('Oops something went wrong! Even all-powerful ancient entities can get things wrong if either you or Skylore messed up.');
	}
});
