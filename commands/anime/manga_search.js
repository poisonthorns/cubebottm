const Jikan = require('jikan-node');
const mal = new Jikan();

module.exports =
{
	name: 'mangasearch',
	description: 'Ping!',
	async execute(message, args)
	{
		if(!args[0])
		{
			return message.reply('Invalid use of command');
		}
		var search = args.join(" ");
		//message.channel.send('Debug: search term: ' + search);
		
		if(search.length < 3)
		{
			return message.reply('Minimum of 3 characters required in search term');
		}
		let fiveSecondTimeout = new Promise(function(resolve, reject) {
				setTimeout(() => reject(new Error('Call timed out after 5 seconds.')), 5000);
		});
		try
		{	
			let searchResults = await Promise.race([mal.search('manga', search, {}), fiveSecondTimeout]);
			const msg = message.channel.send("searching...").then((msg)=>
			{
					//var mangaID = searchResults["results"][0]["mal_id"];
					var currmanga = searchResults["results"][0];
					console.log(currmanga);
					var i = 1;
					while(currmanga != null && currmanga["rated"] === "Rx")
					{
						currmanga = searchResults["results"][i];
						console.log(currmanga);
						++i;
					}
					
					if(currmanga === null)
					{
						msg.edit('search failed');
						msd.react("🤔");
					}
						
					msg.edit('https://myanimelist.net/manga/' + currmanga["mal_id"]);
					msg.react("\u2705");
			})
		}
		catch(err)
		{
			message.channel.send("There was an error.");
		}
	},
};
