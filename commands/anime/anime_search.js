const Jikan = require('jikan-node');
const mal = new Jikan();

module.exports =
{
	name: 'animesearch',
	description: 'Returns the top SFW search result from MAL for the search paramters',
	async execute(message, args, bot)
	{
		if(!args[0])
		{
			return message.reply('Invalid use of command');
		}
		var search = message.toString();
		//message.channel.send('Debug: search term: ' + search);
		
		if(search.length < 3)
		{
			return message.reply('Minimum of 3 characters required in search term');
		}
		const msg = message.channel.send("searching...").then(async (msg)=>
		{
			let fiveSecondTimeout = new Promise(function(resolve, reject) {
					setTimeout(() => reject(new Error('Call timed out after 5 seconds.')), 5000);
			});
			try
			{	
				let searchResults = await Promise.race([mal.search('anime', search, {}), fiveSecondTimeout]);
				
				//var animeID = searchResults["results"][0]["mal_id"];
				var currAnime = searchResults["results"][0];
				console.log(currAnime);
				var i = 1;
				while(currAnime != null && currAnime["rated"] === "Rx" )
				{
					currAnime = searchResults["results"][i];
					console.log(currAnime);
					++i;
				}
				
				if(currAnime === null)
				{
					msg.edit('search failed');
					msd.react("🤔");
				}
					
				msg.edit('https://myanimelist.net/anime/' + currAnime["mal_id"]);
				msg.react("\u2705");
				
			}
			catch(err)
			{
				message.channel.send("There was an error.");
				msg.react("❌");
			}
		})
	},
};
