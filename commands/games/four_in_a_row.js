function takeTurn(message, args, player)
{
	const msg = message.channel.send(":one: | :two: | :three: | :four: | :five: | :six: | :seven:\n--------------------------------------\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:\n:white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle: | :white_circle:").then(async (msg)=>
	{
		msg.react('1⃣')
			.then(() => msg.react('2⃣'))
			.then(() => msg.react('3⃣'))
			.then(() => msg.react('4⃣'))
			.then(() => msg.react('5⃣'))
			.then(() => msg.react('6⃣'))
			.then(() => msg.react('7⃣'))
			.catch(() => console.error('Something messed up on our end.'));
		const filter = (reaction, user) =>
		{
			return ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣'].includes(reaction.emoji.name) && user.id === message.author.id;
		};
		let timeoutFunction = new Promise(function(resolve, reject)
		{
			setTimeout(() => reject(new Error('Call timed out after 5 seconds.')), 7200000);
		});
		
		let reactionWait = new Promise(function(resolve, reject)
		{
			msg.awaitReactions(filter, { max: 1, time: 7200000, errors: ['You timed out'] })
				.then(collected =>
				{
					const reaction = collected.first();

					switch(reaction.emoji.name)
					{
						case '1⃣':
							message.reply('You reacted with 1');
							break;

						case '2⃣':
							message.reply('You reacted with 2');
							break;

						case '3⃣':
							message.reply('You reacted with 3');
							break;

						case '4⃣':
							message.reply('You reacted with 4');
							break;

						case '5⃣':
							message.reply('You reacted with 5');
							break;

						case '6⃣':
							message.reply('You reacted with 6');
							break;

						case '7⃣':
							message.reply('You reacted with 7');
							break;
					}
				})
				.catch(collected =>
				{
					message.reply('You either reacted with an incorrect emoji or your game timed out.');
				});
		});
		
		
		try
		{	
			let searchResults = await Promise.race([reactionWait, timeoutFunction]);
		}
		catch(err)
		{
			message.channel.send("You timed out.");
		}
	})
}

function takeTurn(message, args)
{
	
}

function start(message, args)
{
	if(!args[1] || args[2] != null)
	{
		return message.reply('Incorrect usage of command. Please use \'FourInARow start [player 2\'s username]\'.');
	}
	
	let mention = message.mentions.users.first();
	if(mention == null)
	{
		return message.reply('Please use a real user\'s @.');
	}
	takeTurn(message, args, player)
	
	
}

function end(message, args)
{
	return message.reply('Sorry, this command hasn\'t been implemented yet');
}

module.exports =
{
	name: 'fourinarow',
	description: 'Ping!',
	execute(message, args)
	{
		if(!args[0])
		{
			return message.reply('If you want to start a new game, please use \'FourInARow start [player 2\'s username]\'.\nIf you want to end your current game, please use \'FourInARow end\'.');
		}
		
		switch(args[0])
		{
			case 'start':
				start(message, args);
				break;
				
			case 'end':
				end(message, args);
				break;
		}
	},
};
