module.exports =
{
	name: 'repeat',
	description: '- Returns the arg once for each num',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command');
		if(args[2] != null)
			return message.reply('Too many arguments');
		var num = 0;
		if(!args[1])
		{
			num = 5;
		}
		else
		{
			if(isNaN(args[1]))
				return message.reply("If you provide a second argument, it must be a number.");
			if(args[1] < 1)
				return message.reply("How can you repeat something less than one time? Ever think about that? Huh!?");
			num = args[1];
		}
		if(args[0].length * num > 2000)
			return message.channel.send('Exceeds character limit');
		var i;
		let repeatResult = args[0];
		for(i = 1; i < num; i++)
		{
			repeatResult += " " + args[0];
		}
		message.channel.send(repeatResult);

	},
};
