function chunk(str, n)
{
    var ret = [];
    var i;
    var len;

    for(i = 0, len = str.length; i < len; i += n) {
        ret.push(str.substr(i, n));
    }

    return ret
};

module.exports =
{
	name: 'spacedout',
	description: 'Returns the input backwards',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command');
		var str = args[0]
		var done = !args[1];
		var i = 1;
		while(!done)
		{
			str += " " + args[i];
			i++;
			done = !args[i];
		}
		var spacedOutMessage = (chunk(str, 1).join(" ")).toUpperCase();
		if(spacedOutMessage.length > 2000)
			return message.reply('Message is too long. Please try again with a shorter message.');
		message.channel.send(spacedOutMessage);
	},
};
