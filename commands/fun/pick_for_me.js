module.exports =
{
	name: 'pickforme',
	description: 'Will pick one random choice from the list of arguments',
	execute(message, args, bot)
	{
		if(!args[0])
            return message.reply('Invalid use of command');
		var total = args.length;
		var result = Math.floor(Math.random() * total);
		//message.channel.send('Debug: Running pick_for_me');
		message.channel.send("You should choose this: " + args[result]);
	},
};
