module.exports =
{
	name: 'clap',
	description: 'Returns the arg with the clap emote between each word',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.channel.send(':clap::clap::clap::clap::clap:');
		if(args.length > 197)
			return message.reply('Message too long. Exceeds word limit.');

		var len = args.length;
		var i;
		let clapResult = '';
		for(i = 0; i < len; i++)
		{
			if(clapResult.length > 2000)
			{
				return message.reply('Message too long. Exceeds character limit.');
			}
			clapResult += ":clap:" + args[i];
		}
		clapResult += ":clap:"
		if(clapResult.length > 2000)
		{
			return message.reply('Message too long. Exceeds character limit.');
		}
		message.channel.send(clapResult);
	},
};
