module.exports =
{
	name: 'reverse',
	description: '- Returns the input backwards',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command');

		var str = args[0]
		var done = !args[1];
		var i = 1;
		while(!done)
		{
			str += " " + args[i];
			i++;
			done = !args[i];
		}

		var splitString = str.split("");
		var reverseArray = splitString.reverse(); 
		var joinArray = reverseArray.join("");
		message.channel.send(joinArray);
	},
};
