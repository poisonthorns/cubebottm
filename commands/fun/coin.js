module.exports =
{
	name: 'coin',
	description: 'If you arg specified, flips one coin\n- If arg specified, flips that many coins, shows inidividual results, total of each heads and tails, and how \'lucky\' you were (this one is not implemented yet)',
	execute(message, args, bot)
	{
		if(args[1] != null)
			return message.reply('Too many arguments');
		if(!args[0])
		{
			var result = Math.floor(Math.random() * 2);
			if(result === 0)
				message.channel.send("Heads");
			else
				message.channel.send("Tails");
		}
		else
		{
			if(isNaN(args[0]))
				return message.reply('Invalid use of command');
			if(args[0] > 10000000)
				return message.reply('Skylore\'s computer is too weak. Please try a smaller number.');
			var curr = 0;
			var total = args[0];
			var headCount = 0;
			var tailCount = 0;
			let messageResult = "";
			if(total < 21)
			{
				for(curr = 0; curr < total; curr++)
				{
					var result = Math.floor(Math.random() * 2);
					if(result == 0)
					{
						headCount++;
						messageResult += "Heads ";
					}
					else
					{
						tailCount++;
						messageResult += "Tails ";
					}
				}
				message.channel.send(messageResult);
			}
			else
			{
				for(curr = 0; curr < total; curr++)
				{
					var result = Math.floor(Math.random() * 2);
					if(result == 0)
					{
						headCount++;
					}
				}
				tailCount = total - headCount;
			}
			
			message.channel.send("Total Heads: " + headCount);
			message.channel.send("Total Tails: " + tailCount);
		}
	},
};
