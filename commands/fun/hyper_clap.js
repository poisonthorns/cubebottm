function chunk(str, n)
{
    var ret = [];
    var i;
    var len;

    for(i = 0, len = str.length; i < len; i += n) {
        ret.push(str.substr(i, n));
    }

    return ret
};

module.exports =
{
	name: 'hyperclap',
	description: 'Returns the arg with the clap emote between each letter',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.channel.send(':clap::clap::clap::clap::clap::clap::clap::clap::clap::clap::clap::clap::clap::clap::clap:');
		var str = args[0]
		var done = !args[1];
		var i = 1;
		while(!done)
		{
			str += " " + args[i];
			i++;
			done = !args[i];
		}
		//message.channel.send("Length before chunking: " + str.length);
		if(str.length > 197)
		{
			return message.reply('Argument exceeds character limit. Try sending a shorter message.');
		}
		//message.channel.send('Debug');
		var strResult = chunk(str, 1);
		//message.channel.send('Debug');
		//message.channel.send("Length after chunking: " + strResult.length);

		strResult = strResult.join(":clap:");
		//message.channel.send("Length after joining with claps: " + strResult.length);
		
		var newMessage = ":clap:" + strResult + ":clap:";
		//const HyperClapEmbed = new Discord.MessageEmbed().setDescription(strResult);
		//message.channel.send(HyperClapEmbed);
		message.channel.send(newMessage);
	},
};
