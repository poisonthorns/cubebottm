module.exports =
{
	name: 'ping',
	description: 'Ping!',
	execute(message, args, bot)
	{
		if(args[0] != null)
			return message.reply('Too many arguments');
		message.channel.send('pong!');
	},
};
