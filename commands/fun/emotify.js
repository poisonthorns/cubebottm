let emoteMap = new Map();
emoteMap.set('a', ':regional_indicator_a:');
emoteMap.set('A', ':regional_indicator_a:');
emoteMap.set('b', ':regional_indicator_b:');
emoteMap.set('B', ':regional_indicator_b:');
emoteMap.set('c', ':regional_indicator_c:');
emoteMap.set('C', ':regional_indicator_c:');
emoteMap.set('d', ':regional_indicator_d:');
emoteMap.set('D', ':regional_indicator_d:');
emoteMap.set('e', ':regional_indicator_e:');
emoteMap.set('E', ':regional_indicator_e:');
emoteMap.set('f', ':regional_indicator_f:');
emoteMap.set('F', ':regional_indicator_f:');
emoteMap.set('g', ':regional_indicator_g:');
emoteMap.set('G', ':regional_indicator_g:');
emoteMap.set('h', ':regional_indicator_h:');
emoteMap.set('H', ':regional_indicator_h:');
emoteMap.set('i', ':regional_indicator_i:');
emoteMap.set('I', ':regional_indicator_i:');
emoteMap.set('j', ':regional_indicator_j:');
emoteMap.set('J', ':regional_indicator_j:');
emoteMap.set('k', ':regional_indicator_k:');
emoteMap.set('K', ':regional_indicator_k:');
emoteMap.set('l', ':regional_indicator_l:');
emoteMap.set('L', ':regional_indicator_l:');
emoteMap.set('m', ':regional_indicator_m:');
emoteMap.set('M', ':regional_indicator_m:');
emoteMap.set('n', ':regional_indicator_n:');
emoteMap.set('N', ':regional_indicator_n:');
emoteMap.set('o', ':regional_indicator_o:');
emoteMap.set('O', ':regional_indicator_o:');
emoteMap.set('p', ':regional_indicator_p:');
emoteMap.set('P', ':regional_indicator_p:');
emoteMap.set('q', ':regional_indicator_q:');
emoteMap.set('Q', ':regional_indicator_q:');
emoteMap.set('r', ':regional_indicator_r:');
emoteMap.set('R', ':regional_indicator_r:');
emoteMap.set('s', ':regional_indicator_s:');
emoteMap.set('S', ':regional_indicator_s:');
emoteMap.set('t', ':regional_indicator_t:');
emoteMap.set('T', ':regional_indicator_t:');
emoteMap.set('u', ':regional_indicator_u:');
emoteMap.set('U', ':regional_indicator_u:');
emoteMap.set('v', ':regional_indicator_v:');
emoteMap.set('V', ':regional_indicator_v:');
emoteMap.set('w', ':regional_indicator_w:');
emoteMap.set('W', ':regional_indicator_w:');
emoteMap.set('x', ':regional_indicator_x:');
emoteMap.set('X', ':regional_indicator_x:');
emoteMap.set('y', ':regional_indicator_y:');
emoteMap.set('Y', ':regional_indicator_y:');
emoteMap.set('z', ':regional_indicator_z:');
emoteMap.set('Z', ':regional_indicator_z:');
emoteMap.set('0', ':zero:');
emoteMap.set('1', ':one:');
emoteMap.set('2', ':two:');
emoteMap.set('3', ':three:');
emoteMap.set('4', ':four:');
emoteMap.set('5', ':five:');
emoteMap.set('6', ':six:');
emoteMap.set('7', ':seven:');
emoteMap.set('8', ':eight:');
emoteMap.set('9', ':nine:');
emoteMap.set('?', ':grey_question:');
emoteMap.set('!', ':grey_exclamation:');
emoteMap.set('.', ':white_small_square:');
emoteMap.set('-', ':heavy_minus_sign:');
emoteMap.set('+', ':heavy_plus_sign:');
emoteMap.set('*', ':heavy_multiplication_x:');
emoteMap.set('$', ':heavy_dollar_sign:');
emoteMap.set(' ', '  ');
//These character aren't added yet
//~`@#$%^&*()-_=+[]{}|\;:'"<,>/
module.exports =
{
	name: 'emotify',
	description: 'Returns the arg spelled with emotes',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command');

		var input = args.join(" ");
		var inputLength = input.length;
		var whitespaceCharacters = args.length - 1;
		
		//message.channel.send("Debug: past argument tests");

		let emoteOutput = '\u200b';
		var i = 0;
		var done = false;
		var maxLength = 1999;
		//message.channel.send("Debug: before conversion loop");
		while((i < inputLength) && !done)
		{
			var temp = input[i];
			if(emoteMap.has(temp))
			{
				var emote = emoteMap.get(temp);
				maxLength -= emote.length;
				if(maxLength >= 0)
					emoteOutput += emote;
				else
					return message.reply("Message exceeds character limit. Try again with a shorter message.");
			}
			else
			{
				maxLength -= 1;
				if(maxLength >= 0)
					emoteOutput += temp;
				else
					return message.reply("Message exceeds character limit. Try again with a shorter message.");
			}

			i++;
		}
		//message.channel.send("Debug: after conversion loop");

		message.channel.send(emoteOutput);

	},
};

