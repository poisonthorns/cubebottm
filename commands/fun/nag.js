module.exports =
{
	name: 'nag',
	description: 'If no name handle, fail\n- If no num arg, @handle once\n- If a number is specified, send that many @\'s to Riley',
	execute(message, args, bot)
	{
		if(!args[0])
		{
			return message.reply('Invalid use of command');
		}
		if(args[2] != null)
			return message.reply('Too many arguments');
		var num = 3;
		if(!(!args[1]))
		{
			if(args[1] > 10 || args[1] < 1)
			{
				message.reply('Argument range: 1 <= arg <= 10')
				return;
			}
			num = args[1];
		}

		let mention = message.mentions.users.first();
		if (mention)
		{
			let userMention = "<@" + mention.id + ">";
			message.channel.send(userMention);
			var i = 1;
			
			if(i < num)
			{
				function myLoop()
				{
					setTimeout(function()
					{
						const msg = message.channel.send(userMention).then(async (msg)=>
						{
							msg.delete();
						})
						
						i++;
						
						if(i < num)
						{
							myLoop();
						}
					}, 2000)
				}

				myLoop(); 
			}
		}
		else
		{
			message.reply('Please provide an appropriate handle')
		}
		return;
	},
};
