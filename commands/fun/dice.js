module.exports =
{
	name: 'dice',
	description: 'Rolls [num] dice with [sides] number of sides and outputs both the individual dice and the total',
	execute(message, args, bot)
	{
		if(args[1] != null)
			return message.reply('Too many arguments');
		let dice = [];
		if(args.length === 3)
		{
			//message.channel.send('Debug. Used 3 arguments')
			if(!args[0] || isNaN(args[0]))
				return message.reply('Invalid use of command');
			if(!(args[1] === "d"))
				return message.reply('Invalid use of command');
			if(!args[2] || isNaN(args[2]))
				return message.reply('Invalid use of command');
			dice.push(args[0]);
			dice.push(args[2]);
		}
		else if(args.length === 1)
		{
			//message.channel.send('Debug. Used 1 argument')
			let a = args[0].match(/^(\d{1,5})d(\d{1,5})$/);
			if(a)
			{
				//message.channel.send('Debug. a[0]: ' + a[0] + '. a[1]: ' + a[1] + '. a[2]: ' + a[2]);
				dice.push(a[1]);
				dice.push(a[2]);

			}
			else
			{
				return message.reply('Invalid use of command');
			}
		}
		else
		{
			return message.reply('Invalid use of command');
		}


		//message.channel.send('Debug. dice[0]: ' + dice[0] + '. dice[1]: ' + dice[1]);
		var total = 0;
		var numDice = dice[0];
		var sides = dice[1];
		//message.channel.send('Debug. numDice: ' + numDice + '. sides: ' + sides);
		if((numDice * sides) > 10000 || numDice > 250 || numDice <= 0 || sides <= 0)
			return message.reply('One or both arguments are too large or too small');
		var i;
		let messageResult = "";
		
		for(i = 0; i < numDice - 1; i++)
		{
			var result = Math.floor(Math.random() * sides + 1);
			messageResult += result + " + ";
			total += parseInt(result);
		}
		var fenceResult = Math.floor(Math.random() * sides + 1);
		messageResult += fenceResult;
		total += parseInt(fenceResult);
		if(numDice != 1)
			messageResult += " = " + total;
		message.channel.send(messageResult);
	},
};
