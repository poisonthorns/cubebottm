module.exports = {
	//Utility
	help: require('./utility/help'),
	rule: require('./utility/rules'),
	lmgtfy: require('./utility/lmgtfy'),
	commandcount: require('./utility/command_count'),
	showschedulebecauseiamtoolazytocheckschedule: require('./utility/show_schedule'),
	suggest: require('./utility/suggest'),
	//Anime
	animesearch: require('./anime/anime_search'),
	mangasearch: require('./anime/manga_search'),
	//Fun
	ping: require('./fun/ping'),
	nag: require('./fun/nag'),
	dice: require('./fun/dice'),
	coin: require('./fun/coin'),
	pickforme: require('./fun/pick_for_me'),
	reverse: require('./fun/reverse'),
	clap: require('./fun/clap'),
	hyperclap: require('./fun/hyper_clap'),
	spacedout: require('./fun/spaced_out'),
	emotify: require('./fun/emotify'),
	//1337: require('./fun/1337'),
	repeat: require('./fun/repeat'),
};
