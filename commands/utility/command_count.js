const SQLite = require("better-sqlite3");
const sqlCommandCount = new SQLite('./data/commandCount.sqlite');

module.exports =
{
	name: 'commandcount',
	description: 'Returns the nummber of times the command has been called',
	execute(message, args, bot)
	{
		if(!args[0])
		{
			return message.reply('Invalid use of command');
		}
		
		if(args[1] != null)
		{
			return message.reply('Too many arguments');
		}
		
		var command = args[0].toLowerCase();
		
		if(command === 'rec')
		{
			let count = bot.getCommandCount.get(command);
			if(!count)
			{
				return message.reply('Oops! Let Skylore know he messed up.');
			}
			else
			{
				count.uses --;
				message.channel.send('Various versions of REC or <:REC:690020789109522462> have been said by non-bots ' + count.uses + ' times since July 12th, 2020!');
				bot.setCommandCount.run(count);
				return;
			}
		}

		//message.channel.send('Debug: command: ' + args[0]);
		let count = bot.getCommandCount.get(command);
		if(!count)
		{
			return message.reply('The command either does not exist or has not been called yet.');
		}
		else
		{
			return message.channel.send('The command ' + command + ' has been run ' + count.uses + ' times.');
		}
	},
};
