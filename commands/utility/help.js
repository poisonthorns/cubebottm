const Discord = require('discord.js');
module.exports =
{
	name: 'help',
	description: 'Get a list of command if no args specified\n- Get a description of a command if specified as an arg',
	execute(message, args,bot)
	{
		if(args[1] != null)
			return message.reply('Too many arguments');
		if(!args[0])
		{
			const HelpEmbed = new Discord.MessageEmbed()
				.setTitle('Specific help commands')
				.setColor('#0099ff')
				.addField("Utility", 'Use \"!help utility\" for a list of utility commands')
				.addField("Anime", 'Use \"!help anime\" for a list of anime commands')
				.addField("Fun", 'Use \"!help fun\" for a list of fun commands')
			return message.channel.send(HelpEmbed);
		}
		var msg = args[0].toLowerCase();
		switch(msg)
		{
			case "utility":
				const UtilitedEmbed = new Discord.MessageEmbed()
					.setTitle('The Utility Commands')
					.setColor('#0099ff')
					.addField("CommandCount [name of command]", "Returns the number of times that the specified command has been run")
					.addField("rule [number]", "Returns a brief summary of the specified rule")
					.addField("ShowScheduleBecauseIAmTooLazyToCheckSchedule", "Returns the last thing sent in the schedule channel. Hopefully a schedule.")
					.addField("suggest [suggestion]", "Logs the suggestion for the club and sends the suggestion anonymously to the suggestions channel")
					.addField("LMGTFY [@handle of a user]", "Returns a LMGTFY link for the last things sent in the chat by the specified user")
					.addField("LMGTFY [args]", "Returns a LMGTFY link using the args");
				return message.channel.send(UtilitedEmbed);
				break;
			case "anime":
				const AnimeEmbed = new Discord.MessageEmbed()
					.setTitle('The Anime Commands')
					.setColor('#0099ff')
					.addField("animesearch [search terms]", "Searches MyAnimeList for the search terms and returns a link to the result. Minimum of 3 characters required.")
				return message.channel.send(AnimeEmbed);
				break;
			case "fun":
				const FunEmbed = new Discord.MessageEmbed()
					.setTitle('The Fun Commands')
					.setColor('#0099ff')
					.addField("ping", "pong!")
					.addField("nag [@handle of user] [number]", "If no name handle, it fails. If no number argument, @handle three times. If a number is specified, send that many @'s to @handle")
					.addField("dice [number of dice]d[number of sides]", "Rolls [number of dice] dice with [number of sides] number of sides and outputs both the individual dice and the total")
					.addField("coin [number]", "If no number specified, flips one coin. If number specified, flips that many coins, shows inidividual results (if flipping 20 coins or less) and the total of each heads and tails")
					.addField("PickForMe [text] [text] ...", "Will pick one random choice from the list of arguments")
					.addField("reverse [text]", "Returns the input backwards")
					.addField("repeat [one word] [number]", "Returns the word once for each number")
					.addField("clap [text]", "Returns the arg with the clap emote between each word. If no args provided, returns 5 claps.")
					.addField("HyperClap [text]", "Returns the arg with the clap emote between each letter. If no args provided, returns 10 claps.")
					.addField("SpacedOut [text]", "Returns the arg spaced out and capitalized like this: E X A M P L E")
					.addField("emotify [text]", "Returns the arg spelled with emotes. It cuts off at the message length limit.");
				return message.channel.send(FunEmbed);
				break;
			/************************************************/
			/*              Individual Commands             */
			/************************************************/
			case "animesearch":
				const AnimeSearchEmbed = new Discord.MessageEmbed()
					.setTitle('Anime Search')
					.setColor('#0099ff')
					.addField("Description", "Returns the top SFW result of MyAnimeList.")
					.addField("Argument 1", "A text string of at least length 3. Can be multiple words.")
					.addField("Output", "A link to the MyAnimeList page for the result of the search.");
				return message.channel.send(AnimeSearchEmbed);
				break;
			case "clap":
				const ClapEmbed = new Discord.MessageEmbed()
					.setTitle('Clap')
					.setColor('#0099ff')
					.addField("Description", "Put emphasis on something with claps.")
					.addField("No Arguments", "If you specify no arguments, it returns 5 clap emojis.")
					.addField("Argument 1. Optional.", "A text string. Can be multiple words.")
					.addField("Output", "The text string with a clap emoji between each word.");
				return message.channel.send(ClapEmbed);
				break;
			case "coin":
				const CoinEmbed = new Discord.MessageEmbed()
					.setTitle('Coin')
					.setColor('#0099ff')
					.addField("Description", "Flips a coin.")
					.addField("No Arguments", "Flips a single coin.")
					.addField("Argument 1. Optional.", "A number. Flips that many coins.")
					.addField("Output", "The total number of heads and tails.");
				return message.channel.send(CoinEmbed);
				break;
			case "dice":
				const DiceEmbed = new Discord.MessageEmbed()
					.setTitle('Dice')
					.setColor('#0099ff')
					.addField("Description", "Rolls some dice.")
					.addField("Argument 1", "A number followed by the letter d followed be a second number. The first number is the number of to be rolled, and the second number is the number of sides on the dice. Example: 3d6")
					.addField("Output", "The results of the dice and the total.");
				return message.channel.send(DiceEmbed);
				break;
			case "emotify":
				const EmotifyEmbed = new Discord.MessageEmbed()
					.setTitle('Emotify')
					.setColor('#0099ff')
					.addField("Description", "Emotify your text.")
					.addField("Argument 1", "A text string. Can be multiple words.")
					.addField("Output", "The text string spelled out in emojis.");
				return message.channel.send(EmotifyEmbed);
				break;
			case "hyperclap":
				const HyperClapEmbed = new Discord.MessageEmbed()
					.setTitle('Hyper Clap')
					.setColor('#0099ff')
					.addField("Description", "Put *a lot* of emphasis on something with claps.")
					.addField("No Arguments", "Outputs 10 clap emojis.")
					.addField("Argument 1. Optional.", "A text string. Can be multiple words.")
					.addField("Output", "The text string with an emoji between every character.");
				return message.channel.send(HyperClapEmbed);
				break;
			case "nag":
				const NagEmbed = new Discord.MessageEmbed()
					.setTitle('Nag')
					.setColor('#0099ff')
					.addField("Description", "Nags somebody.")
					.addField("Argument 1", "A users Discord handle. Include the @ sign!")
					.addField("Argument 2. Optional.", "A number. The number of times to tag the user.")
					.addField("Output", "A number of individual pings to the user specified. Defaults to 3 if argument 2 isn't specified. Bot deletes all but one of the @'s as well.");
				return message.channel.send(NagEmbed);
				break;
			case "pickforme":
				const PickForMeEmbed = new Discord.MessageEmbed()
					.setTitle('Pick For Me')
					.setColor('#0099ff')
					.addField("Description", "Makes a choice for you.")
					.addField("Argument 1", "A list of things. Each thing is one word.")
					.addField("Output", "Picks one of the things at random.");
				return message.channel.send(PickForMeEmbed);
				break;
			case "ping":
				const PingEmbed = new Discord.MessageEmbed()
					.setTitle('Ping')
					.setColor('#0099ff')
					.addField("Description", "Check to see if CubeBot:TM: is responsive.")
					.addField("Output", "pong!");
				return message.channel.send(PingEmbed);
				break;
			case "repeat":
				const RepeatEmbed = new Discord.MessageEmbed()
					.setTitle('Repeat')
					.setColor('#0099ff')
					.addField("Description", "Repeats something for you.")
					.addField("Argument 1", "A text string. Only one word.")
					.addField("Argument 2. Optional.", "A number. The number of times to repeat the word.")
					.addField("Output", "A single message with the word repeated the specific number of times. Repeats it 5 times if no number is specified.");
				return message.channel.send(RepeatEmbed);
				break;
			case "reverse":
				const ReverseEmbed = new Discord.MessageEmbed()
					.setTitle('Reverse')
					.setColor('#0099ff')
					.addField("Description", "Reverse something for you.")
					.addField("Argument 1", "A text string. Can be multiple words.")
					.addField("Output", "The reverse of whatever you send in.");
				return message.channel.send(ReverseEmbed);
				break;
			case "spacedout":
				const SpacedOutEmbed = new Discord.MessageEmbed()
					.setTitle('Spaced Out')
					.setColor('#0099ff')
					.addField("Description", "Spaces something out for you.")
					.addField("Argument 1", "A text string. Can be multiple words.")
					.addField("Output", "Outputs the text in all caps with a space between every character.");
				return message.channel.send(SpacedOutEmbed);
				break;
			case "commandcount":
				const CommandCountEmbed = new Discord.MessageEmbed()
					.setTitle('Command Count')
					.setColor('#0099ff')
					.addField("Description", "Check to see how many times a command has been called.")
					.addField("Argument 1", "The name of a command or rec")
					.addField("Output", "The number of times that command has been called.");
				return message.channel.send(CommandCountEmbed);
				break;
			case "lmgtfy":
				const LMGTFYEmbed = new Discord.MessageEmbed()
					.setTitle('LMGTFY')
					.setColor('#0099ff')
					.addField("Description", "Let me Google that for you!")
					.addField("Argument 1. Option 1.", "A users Discord handle.")
					.addField("Argument 1. Option 2.", "A text string. Can be multiple words.")
					.addField("Output for Option 1", "A LMGTFY link for the last message the specified user sent.")
					.addField("Output for Option 2", "A LMGTFY link for whatever text was specified.");
				return message.channel.send(LMGTFYEmbed);
				break;
			case "rule":
				const RuleEmbed = new Discord.MessageEmbed()
					.setTitle('Rule')
					.setColor('#0099ff')
					.addField("Description", "A reminder of the rules.")
					.addField("Argument 1", "A number.")
					.addField("Output", "A brief summary of the specified rule.");
				return message.channel.send(RuleEmbed);
				break;
			case "showschedule":
			case "showschedulebecauseiamtoolazytocheckschedule":
				const ShowScheduleEmbed = new Discord.MessageEmbed()
					.setTitle('Show Schedule Because I Am Too Lazy To Check #Schedule')
					.setColor('#0099ff')
					.addField("Description", "See the title...")
					.addField("Output", "The last thing said in <#688123264501153907>. Which is probably the schedule?... Maybe?...");
				return message.channel.send(ShowScheduleEmbed);
				break;
			case "suggest":
				const SuggestEmbed = new Discord.MessageEmbed()
					.setTitle('Suggest')
					.setColor('#0099ff')
					.addField("Description", "Suggest an anime for us to watch.\nYou can DM the bot this one! Just click CubeBot:TM: in the list of users and message them \"!suggest ...\".")
					.addField("Argument 1", "A text string. Can be multiple words.")
					.addField("Output", "Adds the suggestion to a file for the officers and reposts the suggestion anonymously in <#689273973791391860>.");
				return message.channel.send(SuggestEmbed);
				break;
			default:
				return message.reply("Invalid use of command");
				break;
		}
	},
};
