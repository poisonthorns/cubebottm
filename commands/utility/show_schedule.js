module.exports =
{
	name: 'showschedulebecauseiamtoolazytocheckschedule',
	description: 'Returns the last message sent in the schedule channel',
	async execute(message, args, bot)
	{
		if(args[0] != null)
			return message.reply('Don\'t specify any parameters');
		if(args[1] != null)
			return message.reply('Too many arguments');
		
		/*let schedule = bot.channels.cache.get('688123264501153907').lastMessage;
		if(schedule == null)
			return message.reply('no message has been sent since I\'ve been online. Click <#688123264501153907> you lazy platypus!');
		let scheduleMessage = 'The schedule is:\n\n' + schedule.toString();;
		message.channel.send(scheduleMessage);*/
		var worked = false;
		let scheduleMessages = bot.channels.cache.get('688123264501153907').messages;
		let lastSchedule = await scheduleMessages.fetch({ limit: 10 })
			.then(messages => 
			{
				worked = true;
				//console.log(`Received ${messages.size} messages`);
				message.channel.send('The last thing sent in <#688123264501153907>:\n' + messages.first().toString());
				return;
			})
			.catch(console.error);
		if(worked == false)
			message.channel.send('Something went wrong. Just click <#688123264501153907> you lazy platypus!');
	},
};
