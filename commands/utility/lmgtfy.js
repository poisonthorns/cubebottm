module.exports =
{
	name: 'lmgtfy',
	description: 'Returns a lmgtfy link',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command');
		let mention = message.mentions.users.first();
		if(mention)
		{
			var userID = message.mentions.users.first().id;
			message.channel.messages.fetch()
				.then(messages => {
					var input = messages.filter(m => m.author.id === userID).first().content;
					//message.channel.send('Debug no change: ' + input);
					input = input.replace(/[\s]/g, "+");
					//message.channel.send('Debug replace +: ' + input);
					input = input.replace(/[^0-9a-z\+]/gi, '');
					//message.channel.send('Debug remove special characters: ' + input);
					
					let lmgtfy_link = "https://lmgtfy.com/?q=";
					lmgtfy_link += input;
					message.channel.send(lmgtfy_link);
				})
				.catch(console.error);
			//if(input == null)
			//	return message.channel.send('That user has not sent a message.');
			
		}
		else
		{
			//message.channel.send('Debug: did not mentioned a user');
			var done = !args[0];
			var i = 0;
			while(!done)
			{
				args[i] = args[i].replace(/[^0-9a-z]/gi, '');
				i++;
				done = !args[i];
			}
	
			var input = args.join("+");
			//message.channel.send('Debug: ' + input);
			let lmgtfy_link = "https://lmgtfy.com/?q=";
			lmgtfy_link += input;
			message.channel.send(lmgtfy_link);
		}
	},
};
