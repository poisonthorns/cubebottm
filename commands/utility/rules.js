module.exports =
{
	name: 'rule',
	description: 'Outputs the specified rule',
	execute(message, args, bot)
	{
		if(!args[0])
			return message.reply('Invalid use of command: Please specify which rule');
		if(args[1] != null)
			return message.reply('Too many arguments');
		var msg = '';
		switch(args[0])
		{
			case '-1':
				message.channel.send('sgnineercs gnirud roivaheb evitpursid ro gnitcartsid rehto ro gniklat oN');
				return;
				break;
			case '0':
				message.reply('Don\'t talk about rule 0.')
					.then(specialMessage => {
						specialMessage.delete({ timeout: 2000 })
					})
					.catch(console.error)
				message.delete({ timeout: 2000 });
				return;
				break;
			case '1':
				msg += 'No talking or other distracting or disruptive behavior during screenings';
				break;
			
			case '2':
				msg += 'Don’t tamper with UT or Club property';
				break;
				
			case '3':
				msg += 'Don’t leave trash behind';
				break;
				
			case '4':
				msg += 'Don’t wander the halls';
				break;
			
			case '5':
				msg += 'Don’t disturb other groups that might be meeting nearby';
				break;
			
			case '6':
				msg += 'Don’t prop open the building’s outside doors';
				break;
				
			case '7':
				msg += 'No open flames';
				break;
				
			case '8':
				msg += 'Don’t walk in front of the room';
				break;

			case '9':
				msg += 'Guard your possessions';
				break;
				
			case '10':
				msg += 'Place suggestions into The Cube™';
				break;
				
			case '11':
				msg += 'Please don\'t send anything inappropriate, make anyone uncomfortable, etc.\nThis includes lewd images, demeaning remarks, inappropriate text messages in chat, and many other things not listed here.';
				break;
			
			case '34':
				message.channel.send('That\'s not very family friendly content of you');
				return;
				break;
				
			case '69':
				message.channel.send(':regional_indicator_n::regional_indicator_i::regional_indicator_c::regional_indicator_e:');
				return;
				break;
				
			case '420':
				message.channel.send('https://www.youtube.com/watch?v=DJfg39WkMvE');
				return;
				break;
				
			default:
				message.channel.send('That is not a valid rule. Refer to <#688118946292957399>');
				return;
				break;
		}
		
		msg += '\n\nPlease refer to <#688118946292957399> for more information about each rule.';
		message.channel.send(msg);
	},
};
