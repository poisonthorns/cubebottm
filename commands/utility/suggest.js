module.exports =
{
	name: 'suggest',
	description: 'Adds a suggestion to the suggest sql database and sends an anonymous message in the suggest chat',
	execute(message, args, bot)
	{
		if(!args[0])
		{
			return message.reply('Invalid use of command');
		}
		
		let suggestionChannel = bot.channels.cache.get('689273973791391860')
		if(suggestionChannel === undefined)
			message.channel.send('Error. If you see this, contact a server mod. The programmer messed up');
		var msg = message.toString().substring(message.toString().indexOf(" ") + 1);
		suggestionChannel.send('Anonymous suggestion: ' + msg);
		msg = msg.toLowerCase();
		let suggestion = bot.getSuggestion.get(msg);
		var date = new Date();
		var dateString = date.toString();
		if(!suggestion)
		{
			var randomID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
			suggestion = { id: `${message.id}-${randomID}`, suggestion: msg, total: 1, date_last_suggested: `${dateString}`};
		}
		else
		{
			suggestion.total++;
			suggestion.date_last_suggested = `${dateString}`;
		}
		bot.setSuggestion.run(suggestion);
		message.channel.send('Suggestion received!');
		message.react('✅');
		
	},
};
