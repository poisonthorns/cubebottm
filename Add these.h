case 'repeat':
	if(!args[1])
		return message.channel.send('Invalid use of command');
	var num = 0;
	if(!args[2])
	{
		num = 5;
	}
	else
	{
		if(args[2] < 1)
			return message.reply("How can you repeat something less than one time? Ever think about that? Huh!?");
		num = args[2];
	}
	if(args[1].length * num > 1999)
		return message.channel.send('Exceeds character limit');
	var i;
	let repeatResult = args[1];
	for(i = 1; i < num; i++)
	{
		repeatResult += " " + args[1];
	}
	message.channel.send(repeatResult);
	break;